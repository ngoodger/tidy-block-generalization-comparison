# tidy-block-generalization-comparison

## Name
Tidy Block Generalization Comparison

## Description
Compares the generalization performance on the [tidy-block-env](https://gitlab.com/ngoodger/tidy-block-env).

The experiments are managed using [guild.ai](https://guild.ai/).

## Installation
Tested using Python 3.7.11.

Install dependencies
```
pip install -r requirements.txt
```

Copy the module `tidyblock.py` from [tidy-block-env](https://gitlab.com/ngoodger/tidy-block-env) into the project root directory.

## Usage

### Jupyter-Lab

The notebook `tidy_block_generalization_comparison.ipynb` can be executed normally from `jupyter-lab`.

### Guild Ai

Start guild background queues to run the experiments.  Additional queues can be created if additional gpus are available.

```
guild run queue --background --gpus 0 -y
```

Example Experiments
```
guild run feed_forward:fixed_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run feed_forward:many_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run feed_forward:many_sizes random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run feed_forward:many_sizes_and_colours random_seed='[1]' max_epochs=3000 --stage-trials -y

guild run recurrent_discrete:fixed_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run recurrent_discrete:many_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run recurrent_discrete:many_sizes random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run recurrent_discrete:many_sizes_and_colours random_seed='[1]' max_epochs=3000 --stage-trials -y

guild run seq2seq_generator:fixed_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_generator:many_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_generator:many_sizes random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_generator:many_sizes_and_colours random_seed='[1]' max_epochs=3000 --stage-trials -y

guild run seq2seq_pointer:fixed_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer:many_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer:many_sizes random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer:many_sizes_and_colours random_seed='[1]' max_epochs=3000 --stage-trials -y

guild run seq2seq_pointer_generator:fixed_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer_generator:many_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer_generator:many_sizes random_seed='[1]' max_epochs=3000 --stage-trials -y
guild run seq2seq_pointer_generator:many_sizes_and_colours random_seed='[1]' max_epochs=3000 --stage-trials -y
```
## License
GPL V3 License

## Acknowledgment
This work derives from this [Pytorch Pointer Network Implementation](https://github.com/Guillem96/pointer-nn-pytorch) and I would like to thank Guillem Orellana for sharing and licensing so it could be used.


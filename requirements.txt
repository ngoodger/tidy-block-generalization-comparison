--find-links https://download.pytorch.org/whl/torch_stable.html
jupyter_contrib_nbextensions==0.5.1
torch==1.9.1+cu111
numpy==1.21.2
jupyterlab==3.1.14
guildai==0.7.3
matplotlib==3.4.3
pytest==6.2.5
